# Megakube

> Run Kubernetes locally in a single-node or multiple nodes cluster and simulate the real **Production** environment.


## Installations

* [Vagrant][vagrant] Automate development environments.
* [VirtualBox][virtualbox] A general-purpose full virtualizer.
* [kubectl](docs/installation.md) K8S CLI run on host machine.


## Quick Starts

Start up a three nodes cluster with *1 master node*, and *2 minions nodes*.

Note: *NUM_NODES* value is used to determine how many minions are joined in the cluster.

```bash
# change directory to the *kubeup* folder
cd /PATH/TO/kubeup

# Start up the cluster
export NUM_NODES=2
vagrant up
```

*Optionally, use `NUM_NODES` as inline environment variable. ex: `NUM_NODES=2 vagrant up`*

It should launch three *Linux* *Ubuntu 16.04* machines, with all required *binaries* installed.
*ex: kubectl, kubeadm, kubelet, docker, and much more...*

Note: If you simply want to try out a single-node Kubernetes cluster inside a VM on your laptop like *Minikube* project offers, see this [guide](docs/single-node-cluster.md).


## Work with Kubernetes cluster

The cluster should be up and running after the setup is finished. You could check the machine statuses
with `vagrant status`. All three machines should be in *Running* state.

### Interact with *Kubernetes* cluster from host machine

Set the environment variable once.

```bash
export KUBECONFIG=$(pwd)/shared/k8s.key
```

Use *Kubernetes command-line tool*, [kubectl](docs/installation.md), that you have installed on the
machine, communicate with the *Kubernetes* cluster.

This is the simulation for real world *Kubernetes* cluster running in the cloud, and user can
manages the cluster from his/her work station.

```bash
# show the current cluster information
kubectl cluster-info

# show three nodes in this cluster, 1 master and two minions
kubectl get nodes -o wide

# show all running prods in this cluster
kubectl get pods --all-namespaces -o wide
```

To try out your *Kubernetes* cluster, follow a [deployment guide](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#creating-a-deployment)
on official documentation.

Read on [cluster interaction](docs/cluster-interaction.md) section for more details.


### Tear down the cluster

Shut down the three *Linux* machines, remove them completely.

```bash
./teardown.sh
```


[virtualbox]: https://www.virtualbox.org/
[vagrant]: https://www.vagrantup.com/downloads.html
