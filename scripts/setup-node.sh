#!/bin/bash

###
# Install required softwares and system updates
###

# To void the warning: dpkg-reconfigure: unable to re-open stdin: No file or directory
export DEBIAN_FRONTEND=noninteractive

sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list
apt-get -yqq update
apt-get -y upgrade
apt-get install -y ca-certificates curl apt-transport-https software-properties-common
rm -rf /var/lib/apt/lists/*


###
# Docker Installation
# Docker CE (https://docs.docker.com/install/linux/docker-ce/ubuntu/)
# Docker EE (https://docs.docker.com/install/linux/docker-ee/ubuntu/)
###

# Add Docker’s official GPG key
curl -fsSL $DOCKER_URL/ubuntu/gpg | sudo apt-key add -

# Set up the stable repository
# To add the `edge` or `test` repository, add the word `edge` or `test` (or both)
# after the word `stable` in the commands below.
if [ "$DOCKER_EE_ENABLE" = true ]; then
  add-apt-repository \
    "deb [arch=amd64] $DOCKER_URL/ubuntu \
    $(lsb_release -cs) \
    $DOCKER_EE_VERSION"
else
  add-apt-repository \
    "deb [arch=amd64] $DOCKER_URL/ubuntu \
    $(lsb_release -cs) \
    stable"
fi;


# Update the apt package index
apt-get -yqq update


# Get the official release of the Docker CE/EE version on Ubuntu
DOCKER_CE_VERSION=`apt-cache madison docker-ce | grep $DOCKER_VERSION | head -1 | awk '{print $3}'`
DOCKER_EE_VERSION=`apt-cache madison docker-ee | grep $DOCKER_VERSION | head -1 | awk '{print $3}'`


# Install the latest or specific version of Docker CE or EE
# Check available version: apt-cache madison docker-ce
if $DOCKER_EE_ENABLE ; then
  if [ "$DOCKER_VERSION" == "" ]; then
    apt-get install -y docker-ee
  else
    apt-get install -y docker-ee=$DOCKER_EE_VERSION
  fi;
else
  if [ "$DOCKER_VERSION" == "" ]; then
    apt-get install -y docker-ce
  else
    apt-get install -y docker-ce=$DOCKER_CE_VERSION
  fi;
fi;

# Use Docker as a non-root user
usermod -aG docker vagrant

service docker restart


###
# Install Docker-Compose (https://github.com/docker/compose/releases/)
###

# curl -L https://github.com/docker/compose/releases/download/1.22.0-rc2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
# chmod +x /usr/local/bin/docker-compose


###
# Install Kubeadm (https://kubernetes.io/docs/tasks/tools/install-kubeadm/)
###

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt-get -yqq update

# Find the current available Kubernetes binary version
KUBE_VERSION_DESIRED=`apt-cache madison kubelet | grep $KUBERNETES_VERSION | head -1 | awk '{print $3}'`

# Check if the version is with beta or rc flag, or missing version, etc. If so, use the default K8S version
if [ -z "$KUBE_VERSION_DESIRED" ]
then
  # is empty
  KUBE_VERSION_DEFAULT=`apt-cache madison kubelet | grep $DEFAULT_KUBERNETES_VERSION | head -1 | awk '{print $3}'`

  apt-get install -y kubeadm kubelet=$KUBE_VERSION_DEFAULT kubectl=$KUBE_VERSION_DEFAULT
else
  # is NOT empty
  apt-get install -y kubeadm kubelet=$KUBE_VERSION_DESIRED kubectl=$KUBE_VERSION_DESIRED
fi


###
# Create a K8S cluster (https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/)
###

# kubelet requires swap off
swapoff -a

# keep swap off after reboot
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Configure Kubernetes to use the same CGroup driver as Docker
CGROUP=`docker info 2>/dev/null | sed -n 's/Cgroup Driver: \(.*\)/\1/p'`

# Configure cgroup driver used by kubelet on Master Node
sed -i "0,/ExecStart=/s//Environment=\"KUBELET_EXTRA_ARGS=--cgroup-driver=$CGROUP\"\n&/" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

systemctl daemon-reload
systemctl restart kubelet
