#!/bin/bash

NODES_COUNT=$(echo ${NUM_NODES:-0})

# The command to be used with `config.vm.network "private_network", type: "dhcp"`
# Get the IP address of this VM that VirtualBox has given this VM
IPADDR=`ifconfig enp0s8 | grep Mask | awk '{print $2}'| cut -f2 -d:`

# Pull images used by kubeadm
kubeadm config images --kubernetes-version="${KUBERNETES_VERSION}" pull

# Set up Kubernetes
# --apiserver-advertise-address=<IP adress> — change standard API IP adress.
# --pod-network-cidr=192.168.0.0/16 — needed for bootstraping calico network
kubeadm init --kubernetes-version="${KUBERNETES_VERSION}" --pod-network-cidr=192.168.0.0/16 --apiserver-advertise-address="${IPADDR}" --skip-token-print

# Check if the kubernetes master is up and running
if [ $? -eq 0 ]; then
  # Install a pod network for cluster. a CNI-based pod network add-on. Calico. Installing with the etcd datastore
  # https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/calico
  kubectl --kubeconfig /etc/kubernetes/admin.conf apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
  kubectl --kubeconfig /etc/kubernetes/admin.conf apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
else
  echo [kubeadm init] failed
fi

# make kubectl work for your non-root user, set up admin credentials for vagrant user
if [ ! -f /home/vagrant/.kube/config ]; then
  mkdir -p /home/vagrant/.kube
  cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
  chown vagrant:vagrant /home/vagrant/.kube/config
fi

# Copy admin credentials from `/etc/kubernetes/admin.conf` to host machine
if [ -f /home/vagrant/shared/k8s.key ]; then
  rm -rf /home/vagrant/shared/k8s.key
fi
cp /etc/kubernetes/admin.conf /home/vagrant/shared/k8s.key

# Copy kubeadm join script to host machine for usage
if [ -f /home/vagrant/shared/join.sh ]; then
  rm -rf /home/vagrant/shared/join.sh
fi
SHBANG='#!/bin/bash'
TOKEN=$(kubeadm token create --print-join-command)
printf "${SHBANG}\n${TOKEN}" > /home/vagrant/shared/join.sh

# In a single-node Kubernetes cluster, auto enable K8S taint on master for scheduling workload
if [ "$NODES_COUNT" -eq 0 ]; then
  kubectl --kubeconfig /etc/kubernetes/admin.conf taint nodes --all node-role.kubernetes.io/master-
fi
