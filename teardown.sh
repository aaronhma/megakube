#!/bin/bash
set -e

# Tear down the cluster
vagrant destroy -f

# Remove the cluster info
rm -rf ./shared/k8s.key
rm -rf ./shared/join.sh
rm -rf ./*.log
