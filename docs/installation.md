## [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Install the Kubernetes command-line tool on host machine, to deploy and manage applications on Kubernetes. Using **kubectl**, you can inspect cluster resources; create, delete, and update components; and look at your new cluster and bring up example apps.

- Installation on *MacOS*

1. Download binary directly

```bash
# Download a specific version on macOS. ex: 1.11.0
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/darwin/amd64/kubectl

chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl version
```

```bash
# Or Download the latest release
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
```

2. Download binary via [Homebrew](https://brew.sh/)

```bash
brew install kubernetes-cli
kubectl version
```

- Installation on *Linux*

```bash
# Download a specific version on Linux. ex: 1.11.0
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.11.0/bin/linux/amd64/kubectl

chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl version
```

```bash
# Download the latest release
# curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
```
