## Interact with Kubernetes cluster on host machine

## Join a new node to the current cluster

After you spin up a new node in your local environment, you want the node to join the existing
cluster. For example, the node name is **node-03**,

```bash
# SSH into node-03 node
vagrant ssh node-03
```

Once you are in the *node-03* machine, run the command below, the node should be automatically
discovered by the cluster and joined the cluster.

```bash
/home/vagrant/shared/join.sh
```

Exit the *node-03* machine, log into the master node, check the cluster node status

```bash
# Should see the *node-03* machine is joined the cluster
kubectl get nodes
```

## Remove the entire cluster

```bash
kubeadm reset
```
