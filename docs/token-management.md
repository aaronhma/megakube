## Token

Token hash will be needed for adding workers to the cluster and login to dashboard. Token will be available 24 hours. If token expired, simply create a new token.

- Create new tokens

```bash
kubeadm token create

kubeadm token list
```


### Retrieve the join token and the discovery token

*Kubeadm* join syntax. when you are on the node, you could join the node to the existing cluster via `sudo kubeadm join ${MASTER_IP_ADDR}:{MASTER_IP_PORT} --token ${TOKEN} --discovery-token-ca-cert-hash sha256:${HASH}`

- To retrieve the join token. ex: `TOKEN`

```bash
# retrieve an existing token
kubeadm token list | awk '{print $1}' | xargs echo
```

- To retrieve the discovery token. ex: `HASH`

```bash
# retrieve the discovery token
openssl x509 -in /etc/kubernetes/pki/ca.crt -noout -pubkey | openssl rsa -pubin -outform DER 2>/dev/null | sha256sum | cut -d' ' -f1
```


#### Simply print out the join command again

```bash
# print the join command with a new join token
kubeadm token create --print-join-command
```
