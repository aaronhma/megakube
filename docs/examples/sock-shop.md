## [Weaveworks microservices sock shop](https://microservices-demo.github.io/)

**WIP: Do not try yet**

Clone the [repo](https://github.com/microservices-demo/microservices-demo.git) if needed

```bash
kubectl create namespace sock-shop

# apply the demo to your kubernetes cluster
# https://github.com/microservices-demo/microservices-demo/blob/master/deploy/kubernetes/complete-demo.yaml
kubectl apply -f https://raw.githubusercontent.com/microservices-demo/microservices-demo/master/deploy/kubernetes/complete-demo.yaml

# Check to see if all of your pods are running
kubectl get pods --namespace sock-shop

# Visit http://<ip>:30001/ to see the Sock Shop working
# Visit http://<cluster-ip> (in this case, http://10.110.250.153) with your browser.  You should see the WeaveSocks interface
# additional guide on here. https://www.mirantis.com/blog/how-install-kubernetes-kubeadm/
kubectl get svc front-end -n sock-shop
```
