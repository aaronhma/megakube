## [Kubernetes Dasbhoard](https://github.com/kubernetes/dashboard)

**WIP: Do not try yet**

Dashboard executing with full admin right. Be careful with sharing access to dashboard. Users can rape your cluster.

1. Deploy the Dashboard

Deploy the Kubernetes dashboard to your cluster:

```bash
# Install dashboard, UI with management possibilities
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

Deploy [heapster](https://github.com/kubernetes/heapster.git) to enable container cluster monitoring and performance analysis on your cluster: (metrics collection)

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
```

Deploy the influxdb backend for heapster to your cluster:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
```

Create the heapster cluster role binding for the dashboard:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
```

2. Create an eks-admin Service Account and Cluster Role Binding

By default, the *Kubernetes* dashboard user has limited permissions. create an admin service account
and cluster role binding that you can use to securely connect to the dashboard with admin-level
permissions. For more information, see Managing [Service Accounts](https://kubernetes.io/docs/reference/access-authn-authz/service-accounts-admin/) in the Kubernetes documentation.

Create the admin service account and cluster role binding

```bash
kubectl create -f docs/examples/kubernetes-dashboard/admin-service-account.yaml

kubectl create -f docs/examples/kubernetes-dashboard/admin-cluster-role-binding.yaml
```

3. Connect to the Dashboard

Retrieve an authentication token for the admin service account. Copy the <authentication_token>
value from the output. You use this token to connect to the dashboard.

```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```

Start the kubectl proxy.

```bash
kubectl proxy
```

Open the following link with a web browser to access the dashboard endpoint: [http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/)

Choose Token, paste the <authentication_token> output from the previous command into the
*Token* field, and choose *SIGN IN*.
