## Single node cluster

Similar to [minikube](https://github.com/kubernetes/minikube) project, you can run a single-node Kubernetes cluster inside a VM on your laptop for users looking to try out Kubernetes or develop with it.

```bash
# change directory to the *kubeup* folder
cd /PATH/TO/kubeup

# Start up the cluster
export NUM_NODES=0
vagrant up
```

- SSH into master node

```bash
vagrant ssh master
```

As default, kubernetes does not allow run pod on the master node, or master nodes get a taint which prevents regular workloads being scheduled on the, if you want to allow it, run command bellow

```bash
kubectl taint nodes --all node-role.kubernetes.io/master-
```

Now, you should be able to deploy any workload to a single master node.
