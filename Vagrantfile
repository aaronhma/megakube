# -*- mode: ruby -*-
# vi: set ft=ruby :

# An official K8S release version: ex: '1.10.5', '1.11.2-beta.0', '1.11.0-rc.3'
# Note: beta and rc version may need to add 'DEFAULT_KUBERNETES_VERSION' flag as backward compatible reason
$DEFAULT_KUBERNETES_VERSION=ENV['DEFAULT_KUBERNETES_VERSION'] || '1.11.0'
$KUBERNETES_VERSION=ENV['KUBERNETES_VERSION'] || $DEFAULT_KUBERNETES_VERSION

# `DOCKER_VERSION=''` will download the latest Docker CE version
# After ssh into the machine, `apt-cache madison docker-ce` to see the full list of docker vesrions
# Docker 17.03 is the version that fully tested in the current release of K8S
# The version value requires to be the full major.minor.patch version
$DOCKER_VERSION=ENV['DOCKER_VERSION'] || '17.03.0'

# Use Docker Enterprise Version, default to Docker Community Version
$DOCKER_EE_ENABLE=ENV['DOCKER_EE_ENABLE'] || false

$DOCKER_URL=ENV['DOCKER_URL'] || 'https://download.docker.com/linux'

# Default: run a single-node Kubernetes cluster
$NUM_NODES=ENV['NUM_NODES'] || 0

# The default IP address
$BASE_IP_ADDR=ENV['BASE_IP_ADDR'] || "172.17.8"

# Customize VMs
$vm_gui = false
$vm_memory = 2048
$vm_cpus = 2

$share_home = true

# Enable port forwarding from guest(s) to host machine,
# syntax is: { 80 => 8080 }, auto correction is enabled by default.
$forwarded_ports = {}

VAGRANTFILE_API_VERSION = "2"

# Using the official *Ubuntu* 16.04 linux machine
box = "ubuntu/xenial64"

shared_path = "shared/"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.ssh.forward_agent = true
  # always use Vagrants insecure key
  config.ssh.insert_key = false
  config.vm.box = box
  # config.vm.box_url = box_url

  (0..$NUM_NODES.to_i).each do |i|
    if i == 0
      hostname = "master"
      role = "master"
    else
      hostname = "node-%02d" % i
      role = "node"
    end

    ip = "%s.%d" % [$BASE_IP_ADDR, i+100]

    config.vm.define vm_name = hostname do |config|
      config.vm.hostname = vm_name

      # Disable automatic box update checking. If you disable this, then
      # boxes will only be checked for updates when the user runs
      # `vagrant box outdated`. This is not recommended.
      # config.vm.box_check_update = false

      if $enable_serial_logging
        config.vm.provider :virtualbox do |vb, override|
          vb.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
          vb.customize ["modifyvm", :id, "--uartmode1", serialFile]
        end
      end

      config.vm.provider :virtualbox do |vb|
        # Boot with headless mode or GUI mode
        vb.gui = $vm_gui
        # Use VBoxManage to customize the VM. change memory:
        vb.memory = $vm_memory
        vb.cpus = $vm_cpus
        # vb.destroy_unused_network_interfaces = true
      end

      # Create a forwarded port mapping which allows access to a specific port
      # within the machine from a port on the host machine. Only share ports on master node
      # ex: config.vm.network "forwarded_port", guest: 49156, host: 9876
      if role == "master"
        $forwarded_ports.each do |guest, host|
          config.vm.network "forwarded_port", guest: guest, host: host, auto_correct: true
        end
      end

      if role == "master"
        # VM will have an extra network interface that is assigned by DHCP. ex: `ifconfig`
        config.vm.network "private_network", type: "dhcp"
      else
        config.vm.network "private_network", ip: ip, netmask: "255.255.255.0", auto_config: true, virtualbox__intnet: "k8s-net"
      end


      if $share_home
        config.vm.synced_folder shared_path, "/home/vagrant/shared/"
        # config.vm.synced_folder shared_path, "/home/vagrant/shared/", :mount_options => [ "dmode=777", "fmode=777" ]
      end

      # system "echo '\n********\nCurrent Role: #{role} \nMachine Name: #{hostname}\n********\n'"

      # setup node base and binaries
      config.vm.provision :shell, path: "scripts/setup-node.sh", env: { "DOCKER_VERSION" => $DOCKER_VERSION, "DOCKER_URL" => $DOCKER_URL, "DOCKER_EE_ENABLE" => $DOCKER_EE_ENABLE, "KUBERNETES_VERSION" => $KUBERNETES_VERSION, "DEFAULT_KUBERNETES_VERSION" => $DEFAULT_KUBERNETES_VERSION }

      if role == "master"
        config.vm.provision :shell, path: "scripts/setup-master.sh", env: { "NUM_NODES" => $NUM_NODES, "KUBERNETES_VERSION" => $KUBERNETES_VERSION }
      else
        config.vm.provision :shell, path: "scripts/cluster-join.sh"
      end
    end
  end

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

end
